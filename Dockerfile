FROM tomcat
MAINTAINER george
RUN apt-get update -y && apt-get upgrade -y
copy target/scalatra-maven-prototype.war /usr/local/tomcat/webapps/test.war
EXPOSE 8080
CMD ["catalina.sh", "run"]
